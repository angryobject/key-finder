Attempt to determine the key of a piece of music.

This task appears to be much more involved and mathy. Autocorrelation works more or less only on monophonic pieces.

Some useful stuff:

[Algorithms for determining the key of an audio sample on StackOverflow](http://stackoverflow.com/questions/3141927/algorithms-for-determining-the-key-of-an-audio-sample)
