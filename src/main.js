/* Pitch analysing functions are shamelessly ripped off of Chris Wilson's repo:
** https://github.com/cwilso/PitchDetect/
** I'm yet to understand all this magick myself
*/
(function main() {
   const ctx = new AudioContext();
   const analyser = ctx.createAnalyser();
   const source = ctx.createBufferSource();
   const buffer = new Float32Array(analyser.frequencyBinCount);
   const noteStrings = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];
   const foundNotesElem = document.getElementById('foundNotes')
   const foundNotes = {}

   function fetchAudio() {
      return fetch('./assets/audio.mp3')
         .then(res => res.arrayBuffer())
         .then(arrBuffer => new Promise(resolve =>
            ctx.decodeAudioData(arrBuffer, resolve)
         ));
   }

   function autoCorellate(buf, sampleRate) {
      const size = buf.length;
      const minSamples = 0;
      const maxSamples = Math.floor(size / 2);
      const goodEnoughCorrelation = 0.9;
      const correlations = new Array(maxSamples);
      const rms = (function () {
         let result = 0

         for (let i = 0; i < size ; i++) {
            const val = buf[i];
            result += val * val;
         }

         return Math.sqrt(result / size);
      }());

      // not enough signal
      if (rms < 0.01) {
         return -1;
      }

      let lastCorrelation = 1;
      let bestCorrelation = 0;
      let bestOffset = -1;
      let foundGoodCorrelation = false;

      for (let offset = minSamples; offset < maxSamples; offset++) {
         let correlation = 0;

         for (var i = 0; i < maxSamples; i++) {
            correlation += Math.abs(buf[i] - buf[i + offset]);
         }

         correlation = 1 - (correlation / maxSamples);
         correlations[offset] = correlation; // store it, for the tweaking we need to do below.

         if ((correlation > goodEnoughCorrelation) && (correlation > lastCorrelation)) {
            foundGoodCorrelation = true;

            if (correlation > bestCorrelation) {
               bestCorrelation = correlation;
               bestOffset = offset;
            }
         } else if (foundGoodCorrelation) {
            // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
            // Now we need to tweak the offset - by interpolating between the values to the left and right of the
            // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
            // we need to do a curve fit on correlations[] around bestOffset in order to better determine precise
            // (anti-aliased) offset.

            // we know bestOffset >= 1,
            // since foundGoodCorrelation cannot go to true until the second pass (offset = 1), and
            // we can't drop into this clause until the following pass (else if).
            var shift = (correlations[bestOffset + 1] - correlations[bestOffset - 1]) / correlations[bestOffset];
            return sampleRate / (bestOffset + (8 * shift));
         }
         lastCorrelation = correlation;
      }

      if (bestCorrelation > 0.01) {
         return sampleRate / bestOffset;
      }

      return -1
   }

   function noteFromPitch(frequency) {
      const noteNum = 12 * (Math.log(frequency / 440) / Math.log(2));
      return Math.round(noteNum) + 69;
   }

   function frequencyFromNoteNumber(note) {
	   return 440 * Math.pow(2, (note - 69) / 12);
   }

   function centsOffFromPitch(frequency, note) {
      return Math.floor(1200 * Math.log(frequency / frequencyFromNoteNumber(note)) / Math.log(2));
   }

   function analyse() {
      analyser.getFloatTimeDomainData(buffer);

      const ac = autoCorellate(buffer, ctx.sampleRate);

      // can't detect
      if (ac === -1) {
         requestAnimationFrame(analyse);
         return;
      }

      const pitch = ac;
      const note =  noteFromPitch(pitch);
      const detune = centsOffFromPitch(pitch, note);

      let noteStr = noteStrings[note % 12];

      foundNotes[noteStr] = foundNotes[noteStr] || [];
      foundNotes[noteStr].push(detune);

      // calc average detune & remove entries bigger than that
      const squares = Object.values(foundNotes)
         .reduce((acc, arr) => {
            arr.forEach(v => acc.push(v * v));
            return acc;
         }, []);
      const rms = Math.sqrt(squares.reduce((a, b) => a + b) / squares.length);

      const result = Object.keys(foundNotes).reduce((acc, note) => {
         const l = foundNotes[note]
            .filter(v => (Math.abs(v - rms) / 100) < 0.1)
            .length;

         if (l) {
            acc.push([note, l]);
         }

         return acc;
      }, [])
      .sort((a, b) => b[1] - a[1])
      .reduce((acc, v) => {
         acc[v[0]] = v[1];
         return acc;
      }, {});

      foundNotesElem.innerText = JSON.stringify(result, null, 3);

      requestAnimationFrame(analyse);
   }

   function start(buffer) {
      analyser.fftSize = 2048;
      analyser.connect(ctx.destination);

      source.buffer = buffer;
      source.loop = false;
      source.connect(analyser);

      source.start(0);
      analyse();
   }

   fetchAudio()
      .then(start)
      .catch(err => {
         throw err;
      });
}())
